/**
* Sort a password by
	* uppercase letters
	* lowercase letters
	* numbers
	* special characters
* Char types do not have to be sorted

Pseudocode v1:
1. Find uppercase letters in string and push to temp array.
	* Sort uppercase letters and push to new password array.
2. Find lowercase letters in string and push to temp array.
	* Sort lowercase letters and push to new password array.
3. Find numbers in string and push to temp array.
	* Sort numbers and push to new password array.
4. Sort special characters and push to new password array.
5. Join and return new password array as string.

Pseudocode v2
1. Find uppercase letters in string and add to new string.
2. Find lowercase letters in string and add to new string.
3. Find numbers in string and add to new string.
5. Find special characters and add to new string.
6. Return new string

*/

var normalizePassword = function(password) {
	var letters = 'abcdefghijklmnopqrstuvwxyz';
	var bigLetters = letters.toUpperCase();
	var numbers = '0123456789';
	var specialChars = '()`~!@#$%^&*-+=|\{}[]:;\"\'<>,.?/';
	var password = password.split('');
	var normPass = '';	
	var checkForChars = function(charStr) {
			var i = 0;
			
			while (i < password.length) {
				if (charStr.includes(password[i])) {
					normPass += password.splice(password.indexOf(password[i]), 1);
				} else {
						i++;
				}
			}
	}
	
	checkForChars(bigLetters);
	checkForChars(letters);
	checkForChars(numbers);
	checkForChars(specialChars);
	
	return normPass;
}
var password = '3A12^,^b4';
var normalizedPassword = normalizePassword(password);
console.log(normalizedPassword); // Ab3124^,^