// left: 14
// right: 7
/*
- params: char to pad, length of required chars
- check length of data, if less than required
- calc num of chars to required length
- loop/pad chars to left or right

- make work with various data types
- Watch for types! The type that comes in is the type you must return.
*/
String.prototype.padLeft = function(ch, n) {
  var data = '';

  if (this.length < n) {
    for (var i = 1; i < n; i++) {
      data += ch;
    }
    data += this;
  }
  else {
    data = this;
  }
  return data;
  // return this; How can I make it work with this line?
};

String.prototype.padRight = function(ch, n) {
  var data = this;

  if (this.length < n) {
    for (var i = 1; i < n; i++) {
      data += ch;
    }
  }
  return data;
  // return this;
};

// '2'.padLeft('0', 4);
// console.log('2'.padLeft('0', 4));
console.log('2'.padRight('0', 4));
