// NOTE 8-codewars.js

/*
1. A Needle in the Haystack
Can you find the needle in the haystack?

Write a function findNeedle() that takes an array full of junk but containing one "needle"

After your function finds the needle it should return a message (as a string) that says:

"found the needle at position " plus the index it found the needle

So findNeedle(['hay', 'junk', 'hay', 'hay', 'moreJunk', 'needle', 'randomJunk'])
should return

'found the needle at position 5'

*/

function findNeedle(haystack) {
  for (var i = 0; i < haystack.length; i++) {
    if (haystack[i] == 'needle') {
      return 'found the ' + haystack[i] + ' at position ' + i;
    }
  }
}

/*
2. The 'if' function
Who likes keywords? Nobody likes keywords, so why use them?

You know what keyword I use too much? if! We should make a function called _if, with its arguments
as a logical test and two functions/lambdas where the first function is executed if the boolean
is true, and the second if it's false, like an if/else statement, so that we don't have to mess
around with those nasty keywords! Even so, It should support truthy/falsy types just like the keyword.

Examples:

_if(true, function(){console.log("True")}, function(){console.log("false")})
// Logs 'True' to the console.
*/
function _if(bool, func1, func2) {
  if (bool) {
    return func1();
  } else {
    return func2();
  }
}

/*
3. Vowel remover 1.2
Create a function called shortcut to remove all the lowercase vowels in a given string.

Examples

shortcut("codewars") // --> cdwrs
shortcut("goodbye")  // --> gdby
*/
// shortcut v1
// function shortcut(string){
//   var vowels = 'aeiou';
//   var i = 0;
//
//   while (i < vowels.length && string.length != 0) {
//     // console.log('LOOP:' + i + ' vowel: ' + vowels[i]);
//
//     if (string.includes(vowels[i])) {
//       string = string.replace(vowels[i], '');
//       // console.log(string);
//     } else {
//       i++;
//     }
//   }
//   return string;
// }

// console.log(shortcut("ah hi, hello there"));
// console.log(shortcut('a'));
// console.log(shortcut('aaeee'));
// console.log(shortcut(''));

// shortcut v2
function shortcut(string){
  var vowels = 'aeiou';
  var i = 0;

  while (i < string.length && string.length != 0) {
    if (string.length < vowels.length) {
      if (vowels.includes(string[i])) {
        string = string.replace(string[i], '');
      } else {
        i++;
      }
    }
    else if (string.includes(vowels[i])) {
      string = string.replace(vowels[i], '');
    } else {
      i++;
    }
  }
  return string;
}
// shortcut other versions
function shortcut(string){
  return string.replace(/[aeiou]/gi, '');
}

function shortcut(string){
  var vowels = "aeiou";
  var output = "";
  for (var i = 0; i < string.length; i++) {
    if (!(vowels.indexOf(string[i]) > -1)) {
      output += string[i];
    }
  }
  return output;
}
/*
4. Musical Pitch Classes 1.2
In music, each note is named by its pitch class (e.g., C, E♭, F♯), and each pitch class can alternatively be expressed as an
integer from 0 to 11. Your task will be to write a method called pitch_class (JS: pitchClass ) that, when given a letter-based
pitch class, returns the corresponding integer.

Only seven letters are used to name the notes: "A" through "G." These letter names are cyclical, just like the days of the week.
The notes corresponding to those letters are called the "natural notes." Here are the numbers corresponding to each of them:

C : 0
D : 2
E : 4
F : 5
G : 7
A : 9
B : 11
So pitch_class('D') (JS: pitchClass('D') ) should return 2, and pitch_class('B') (JS: pitchClass('B') ) should return 11.

The sharp sign ("♯") is essentially an increment operator, so "C♯" (pronounced "C sharp") refers to one note higher than C,
which has a value of 1, whereas F♯ has a value of 6. Since Codewars doesn't allow the sharp sign, we'll use a number sign ("#") instead.

The flat sign ("♭") is the opposite of a sharp, meaning one note lower. F♭ has a value of 4, and C♭ has a value of 11
(the twelve-note system is cyclical). Since Codewars doesn't allow the flat sign, we'll use a lowercase "b" instead.

Return nil (JS: null ) for invalid input.
*/
https://www.codewars.com/kata/54d472e98776e4eb5b000215/solutions/javascript
// v1
function pitchClass(note) {
  var notes = 'C D EF G A B';
  // var notes = ['C', , 'D', , 'E', 'F', ,'G' , , 'A', , 'B'];
  for (var i = 0; i < notes.length; i++) {
    if (notes[i] == note) {
      return i;
    }
    else if (notes[i] + '#' == note && i < 11) {
      return i+1;
    }
    else if (notes[i] + 'b' == note && i > 0) {
      return i-1;
    }
    else if (notes[i] + '#' == note || notes[i] + 'b' == note) {
      return (notes.length-1) - i;
    }
  }
  return null;
}

console.log(pitchClass('Cb'));
console.log(pitchClass('C'));
console.log(pitchClass('C#'));
console.log(pitchClass('B#'));

// pitchClass v2
function pitchClass(note) {
  var notes = {'C': 0, 'D': 2, 'E': 4, 'F': 5, 'G': 7, 'A': 9, 'B': 11};
  for (key in notes) {
    if (key == note) {
      return notes[key];
    }
    else if (key + '#' == note && notes[key] != 11) {
      return notes[key] + 1;
    }
    else if (key + 'b' == note && notes[key] != 0) {
      return notes[key] - 1;
    }
    else if (key + '#' == note || key + 'b' == note) {
      return Math.abs(notes[key] - 11);
    }
  }
  return null;
}
console.log(pitchClass('C'));
console.log(pitchClass('C#'));
console.log(pitchClass('Cb'));
console.log(pitchClass('B#'));
