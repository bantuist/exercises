# Goal: return checkered_board
  # even number board returns board that begins with black square
  # odd number board should return board that begins with white square
  # input: num
  # if less than 2, return false
# if num is even
  # save to board string
  # num.times put white square
  # num.times put black square
# if num is odd
  # save to board string
  # num.times put black square
  # num.times put white square
# else
  # false
#

def checkered_board(dimension)
  board = ''

  if dimension.class != Fixnum || dimension < 2
    return false
  elsif dimension.even?
    square1 = "\u25A1 "
    square2 = "\u25A0 "
  else
     square1 = "\u25A0 "
     square2 = "\u25A1 "
  end

  dimension.times do |i|
    row = ''

    if i.even?
      dimension.times do |n|
        if n.even?
          row << square1
        else
          row << square2
        end
      end
    else # i.odd?
      dimension.times do |n|
        if n.even?
          row << square2
        else
          row << square1
        end
      end
    end
    board << "#{row.rstrip}\n"
  end
  # p board
  # p board.rstrip!
  puts
  p board.rstrip!
  puts
end

# puts checkered_board(nil)
# puts checkered_board(1)
checkered_board(2)
checkered_board(3)
# checkered_board(4)
