# Find count of most frequent integer in array

def most_freq_int(integers)
  count  = 0 

  integers.each do |int|
    if integers.count(int) > count
      count = integers.count(int)
    end
  end

  p count

end

integers = [3, -1, -1, -1, 2, 3, -1, 3, -1, 2, 4, 9, 3]
most_freq_int(integers)

def most_frequent_item_count(c)
    c.count(c.max_by{|x| c.count(x)})
end
end

def most_frequent_item_count(collection)
    collection.uniq.map{ |v| collection.count v }.max || 0
end
end
