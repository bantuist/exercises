# each subsequent term is the sum of the previous x terms
  # x is the amount of initial args
# return enumerator that responds to enum methods

# create enumerator based on params
# sum params and save to next
# if i get 0, 1
# I should sum the last two terms of the enum
# Append to enum
# Return num
#

# def sequence_gen(*args)
#   x_args = args.length
#   previous_x_args = args.slice(args.length - x_args, args.length)
#   next_term = previous_x_args.reduce(:+)
#   args.push(next_term)
#
#
#   # args.push(args.slice(args.length - args.length - 2, args.length))
#   # puts args
# end

# http://blog.carbonfive.com/2012/10/02/enumerator-rubys-versatile-iterator/
# http://blog.arkency.com/2014/01/ruby-to-enum-for-enumerator/
# http://mixandgo.com/blog/mastering-ruby-blocks-in-less-than-5-minutes
# https://www.sitepoint.com/implementing-lazy-enumerables-in-ruby/
# https://rossta.net/blog/what-is-enumerator.html
# https://rossta.net/blog/infinite-sequences-in-ruby.html
# https://tomafro.net/2012/12/infinite-sequences-in-ruby


# arbitrary array of arguments passed to sequence_gen
def sequence_gen(*args)
  #
  Enumerator.new do |yielder|
    # An infinite loop that (lazily?) executes the block once each time an iterator method is called on the object, iterating once for each time << (yield) is called on yielder.
    loop do
      # Pushes the sum of args (1) to the end of args ([0,1]) resultingi in [0, 1, 1]
      args.push(args.reduce(:+))
      # stores the instructions (not the value) for the next yield
      # shifts the first element (0) off of args ([0, 1, 1]), resulting in [1, 1]
      # yields the shifted arg as instructed
      yielder << args.shift
    end
  end
end

fib = sequence_gen(0, 1) # returns an Enumerator
puts fib.next #= 0 # first term (provided)
puts fib.next #= 1 # second term (provided)
puts fib.next #= 1 # third term (sum of first and second terms)
puts fib.next #= 2 # fourth term (sum of second and third terms)
# fib.next = 3 # fifth term (sum of third and fourth terms)
# fib.next = 5 # sixth term (sum of fourth and fifth terms)
# fib.next = 8 # seventh term (sum of fifth and sixth terms)

# fib.take(5)
