def stringy(size)
  string = ''
  (0...size).each do |i|
    # What happens when I push numbers instead of strings?
    # Why do I have to start with '0' if odd when 0 is even?
    i.odd? ? string << '0' : string << '1'
  end
  puts string
end

stringy(6)

# Version 2

def stringy(size)
  "10" * (size / 2) + "1" * (size % 2)
end

# Version 3

def stringy(size)
  ('10'*size)[0,size]
end

# Version 4
def stringy(s)
  # '10101010101010101010101010101010101010101'[0...size]
  # lol ^
  "".rjust(s, "10")
end
