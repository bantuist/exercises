# in a 9x9 board
# 1..9 in each row
# 1..9 in each column
# 1..9 in each 3x3

# check that each row contains 1..9
  # loop board[row]
    # includes? 1..9

# check that each column contains 1..9
  # remove number from column as I iterate through each row
  # if the number has already been removed from that column

# check that each 3x3 grid contains 1..9

# create arrays from coluns and 3x3s to check? sub-grid


def validSolution(board)
  valid = true
  tboard = board.transpose
  column = []
  row = []
  subgrid = []

  tboard.each do |arr|
    if column.length < 9
    elsif column.length == 9 && column.sort == (1..9).to_a
      column = []
    else
      valid = false
    end
    arr.each do |n|
      column.push(n)
    end
  end

  board.each do |arr|
    # check row
    if row.length < 9
    elsif row.length == 9 && row.sort == (1..9).to_a
      row = []
    else
      valid = false
    end

    # subgrid check
    if subgrid.length < 9
    elsif subgrid.length == 9 && subgrid.sort == (1..9).to_a
      subgrid = []
    else
      valid = false
    end
    arr.each_with_index do |num, j|
      row.push(num)
      if j < 3
        subgrid.push(num)
      end
    end
  end
  valid
end

puts validSolution([[5, 3, 4, 6, 7, 8, 9, 1, 2],
               [6, 7, 2, 1, 9, 5, 3, 4, 8],
               [1, 9, 8, 3, 4, 2, 5, 6, 7],
               [8, 5, 9, 7, 6, 1, 4, 2, 3],
               [4, 2, 6, 8, 5, 3, 7, 9, 1],
               [7, 1, 3, 9, 2, 4, 8, 5, 6],
               [9, 6, 1, 5, 3, 7, 2, 8, 4],
               [2, 8, 7, 4, 1, 9, 6, 3, 5],
               [3, 4, 5, 2, 8, 6, 1, 7, 9]])
