=begin
Test: return fastest option (walk or bus)
- take 3 params: walking distance home, bus distance, office-bus-stop-home distance.
  - tot_dist: walk_dist bus_dist - tot_dist can equal walk_dist, but walk_dist can be different than passed walk_dist
  - walk_dist: if not taking the bus

- walk 4 km/hr
- bus 8 km/hr
- > 2 hours: bus
- < 10 min: walk
- ==: walk
===========================================================
- calculate walk time
- if walk time is > 2 hours, bus
- if walk time is < 10 min, walk
- else calculate bus time
- compare walk to bus time
- if walk time < bus time, walk
- else bus
- take care of floats
25 min
Next: figure out bus_dist
=end

#def bus_or_walk(tot_dist, bus_dist, walk_dist)
  #$walk_speed = 5.0 # kmh
  #$bus_speed  = 8.0 # kmh
  
  #walk_time = walk_dist.to_f / $walk_speed # hours: t = d/s
  #bus_time = ((tot_dist.to_f - bus_dist) / $walk_speed) + 
    #(bus_dist.to_f / $bus_speed) # hours: t = d/s + d/s

  #p walk_time
  #p walk_time * 60
  #p bus_time
  #if walk_time > 2
    #p "Bus default"
  #elsif walk_time * 60 < 10
    #p "Walk default"
  #elsif bus_time > walk_time
    #p "Bus is faster"
  #else 
    #p "Walk is faster"
  #end
#end

#def bus_or_walk(tot_dist, bus_dist, walk_dist)
  #$walk_speed = 5.0 # kmh
  #$bus_speed  = 8.0 # kmh
  
  #walk_time = walk_dist.to_f / $walk_speed # hours: t = d/s
  #bus_time = ((tot_dist.to_f - bus_dist) / $walk_speed) + 
    #(bus_dist.to_f / $bus_speed) # hours: t = d/s + d/s

  #p walk_time
  #p walk_time * 60
  #p bus_time
  #if walk_time > 2
    #p "Bus default"
  #elsif walk_time * 60 < 10
    #p "Walk default"
  #elsif bus_time > walk_time
    #p "Bus is faster"
  #else 
    #p "Walk is faster"
  #end
#end
# Difficulty: understanding the problem i.e. distance = walking distance,
# bus_drive and bus_walk
# Language comprehension problem
#
def calculator(distance, bus_drive, bus_walk)
  $walk = 5.0
  $bus = 8.0
  walk_time = distance / $walk
  bus_time = (bus_walk / $walk) + (bus_drive / $bus)
  
  if walk_time > 2
    return "Bus default"
  elsif walk_time * 60 < 10
    return "Walk default"
  else
    return walk_time <= bus_time ? "Walk" : "Bus" 
  end
end

##puts calculator(5, 6, 1)

#puts calculator(5, 8, 0)
#puts calculator(5, 4, 3)
puts calculator(11, 15, 2)
puts calculator(0.6, 0.4, 0)
#puts calculator(10, 0.4, 0)
