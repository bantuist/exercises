class Gigasecond
  def self.from(past_time)
    current_time = Time.now.utc
    gigasecond = 1E9 - (current_time - past_time)
    gigasecond_moment = current_time + gigasecond
  end
end

module BookKeeping
  VERSION = 5
end
