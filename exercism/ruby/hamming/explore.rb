def hamming(a, b)
  a.split('') - b.split('')
end

def hamming(a, b)
  (b.chars & a.chars).size - a.size
end
