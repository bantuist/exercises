module BookKeeping
  VERSION = 3
end

# Calculate the Hamming difference between two DNA strands
# Compare two strands and return sum of different nucleotides represented by letters
# Compare by index

class Hamming
  def self.compute(strand1, strand2)
    sum = 0

    if strand1.length != strand2.length
      raise ArgumentError, "Sequences must be equal length."
    elsif strand1 == strand2
      sum
    else
      strand1.split('').each_index do |i|
        if strand1[i] != strand2[i]
          sum += 1
        end
      end
    end
    sum
  end
end

# Hamming.compute('ACCAGGG', 'ACTATGG')
