/* 12. 10 min
Check if number is perfect
Example : The first perfect number is 6, because 1, 2, and 3 are its proper
positive divisors, and 1 + 2 + 3 = 6. Equivalently, the number 6 is equal to
half the sum of all its positive divisors: ( 1 + 2 + 3 + 6 ) / 2 = 6. The next
perfect number is 28 = 1 + 2 + 4 + 7 + 14. This is followed by the perfect
numbers 496 and 8128.

- find divisiors
- check if sum of divisors = number
- check if number = half sum of divisors

*/

// v1 Sum of proper positive divisors
function checkPerfectNumber(num) {
  var sum = 0;
  for (var i = 1; i < num; i++) {
    if (num % i == 0) {
      sum += i;
    }
  }
  if (sum == num && num != 0) {
    return "perfect";
  }
  else {
    return "not perfect";
  }
}

// v2 Half sum of all positive divisors
function checkPerfectNumber(num) {
  var i = 1;
  var check = 0;

  while (i <= num) {
    if (num % i == 0) {
      check += i;
    }
    i++;
  }
  if (check / 2 == num && num != 0) {
    return "perfect";
  }
  else {
    return "not perfect";
  }
}

console.log(checkPerfectNumber(0));
console.log(checkPerfectNumber(1));
console.log(checkPerfectNumber(6));
console.log(checkPerfectNumber(28));
console.log(checkPerfectNumber(496));
console.log(checkPerfectNumber(2389));
console.log(checkPerfectNumber(8128));
