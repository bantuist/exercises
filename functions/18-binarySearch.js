/* 18. ~ 25 + 25 min
Write a function for searching JavaScript arrays with a binary search.
Note : A binary search searches by splitting an array into smaller and smaller chunks until it finds the desired value.
- split array in half
- check first half for number
- check second half for number
*/
// Snapshot 1
// function searchArray(array, number) {
//   var firstHalf = array.slice(0, array.length / 2);
//   console.log(firstHalf);
//   var secondHalf = array.slice(array.length / 2, array.length);
//   console.log(secondHalf);
//
//   for (var i = 0; i < firstHalf.length; i++) {
//     if (firstHalf[i]  == number) {
//       return number;
//     }
//     else if (secondHalf[i] )
//     }
//   }
// }
//
// searchArray([10, 9, 8, 7, 6, 5, 4, 3, 2, 1], 2);

// Snapshot 2
// function searchArray(array, number) {
//   if (array[array.length / 2] == number) {
//     console.log(array[array.length / 2]);
//   } else {
//       for ()
//     var firstHalf = array.slice(0, array.length / 2);
//     console.log(firstHalf);
//     var secondHalf = array.slice(array.length / 2, array.length);
//     console.log(secondHalf);
//   }
//
//
// }
//
// function numberArray(start, stop) {
//   numbers = [];
//   for (var i = start; i <= stop; i++) {
//     numbers.push(i);
//   }
//   return numbers;
// }
//
// function binarySearch(array, num) {
//   array.sort(function(a, b){return a-b});
//   // console.log(array);
//   var min = 0;
//   var max = array.length;
//
//   for (var i = 0; i < array.length; i++) {
//     var guess = Math.floor((min + max) / 2);
//
//     console.log("Guess " + i + ": " + array[guess]);
//     // console.log("   new min index: " + min);
//     // console.log("   new max index: " + max);
//     if (array[guess] == num) {
//       return array[guess] + " is at index " + guess;
//     }
//     else if (array[guess] > num) {
//       max = guess;
//     }
//     else {
//       min = guess;
//     }
//   }
// }
// console.log(binarySearch(numberArray(20, 50), 42));

function binarySearch(array, letter) {
  var i;
  var guess;

  while (array.length > 1) {
    console.log(array);
    i = Math.floor(array.length / 2);
    guess = array[i];
    console.log(array.length);
    console.log(i + ":" + guess);

    if (guess > letter) {
      array = array.splice(0, i)
    }
    else if (guess < letter) {
      array = array.splice(i + 1, array.length - 1);
    }
    else {
      console.log(i);
      return "==:" + guess;
    }
    console.log(array);
  }
  return array[0];
}

var letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p"];
console.log(binarySearch(letters, "b"));


function binarySearch(array, letter) {
  var i;
  var guess;

  while (array.length > 1) {
    i = Math.floor(array.length / 2);
    guess = array[i];

    if (guess > letter) {
      array = array.splice(0, i)
    }
    else if (guess < letter) {
      array = array.splice(i + 1, array.length - 1);
    }
    else {
      return "==:" + guess;
    }
  }
  return array[0];
}

var letters = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p"];
console.log(binarySearch(letters, "b"));
