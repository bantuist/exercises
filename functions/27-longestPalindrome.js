// 27 longest palindrome

// function longestPalindrome(string) {
//   string = string.replace(/ /g, '');
//   var letters = string.replace(' ', '').split('');
//   var current = [];
//   var slice;
//   var longest = '';
//   var i = 0;
//   var j;
//
//   while (i < letters.length) {
//     // console.log("OUTER: " + i);
//     current.push(letters[i]);
//     j = i + 1;
//     while (j < letters.length) {
//       // console.log(" INNER: " + j);
//       current.push(letters[j]);
//       slice = string.slice(i, j+1).split('').reverse().join('');
//       if (current.join('') == slice && slice.length > longest.length){
//         // Fixed mistake: setting longest to current instead of slice
//         // Didn't work the same with nested ifs
//         // Why isn't nurses run being listed?
//         // Because it is the same length as longest
//         longest = slice;
//         console.log("   LONGEST: " + longest);
//
//       }
//       j++;
//     }
//     current = [];
//     i++;
//   }
//   console.log("   LONGEST: " + longest);
// }
//
// longestPalindrome("bananas racecar nurses run")
// // nurses run


/*
- remove whitespace
- while char at first index is not equal to char at last index
  - increment and decrement indices respectively comparing each char
  - return palindrome if found
  - else reset first and last indices, repeat


*/

function longestPalindrome(string) {
  var i = 0;
  var j = string.length - 1;
  var first;
  var last;
  var longest = '';

  while (i != j) {
    first = i;
    last = j;
    while (first != last) {
      console.log("INNER: " + first + last);
      console.log(string[first]);
      console.log(string[last]);
      if (string[first] == string[last]) {
        longest += string[first] + string[first + 1];
        console.log(longest);
      } else {
        longest = '';
      }
      first++;
      last--;
    }
    // if (longest != '') {
    //   console.log(longest);
    //   return longest;
    // } else {
      i++;
      j--;
    // }
  }
  // console.log(longest);
}
longestPalindrome("anana")
                 //01234
// longestPalindrome("bananas racecar nurses run")
