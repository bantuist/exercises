// function countVowels(string) {
//   var vowelCount = 0;
//   var letters = string.toLowerCase().split("");
//   for (var i = 0; i < letters.length; i++) {
//     if (letters[i] == "a" || letters[i] == "e" || letters[i] == "i" || letters[i] == "o" || letters[i] == "u") {
//       vowelCount += 1;
//     }
//   }
//   return vowelCount;
// }

// includes() vs. contains()?
// if (string.includes("e")) { // Why doesn't includes() work?

// function countVowels(string) {
//   var vowelCount = 0;
//   // var letters = string.toLowerCase().split("");
//   for (var i = 0; i < string.length; i++) {
//     return string.includes("e");
//     if (string.includes("e")) {
//       vowelCount++;
//     }
//   }
//   return vowelCount;
//
// }


// indexOf()


// Nested for loops
// function countVowels(string) {
//   var vowelCount = 0;
//   var vowels = "aeiou";
//   for (var i = 0; i < string.length; i++) {
//     for (var j = 0; j < vowels.length; j++) {
//       if (string[i] == vowels[j]) {
//         vowelCount++;
//       }
//     }
//   }
//   return vowelCount;
//
// }
//
// console.log(countVowels("The quick brown fox"));

// v.2 after mentor session

// function countVowels(string) {
//   var letters = string.split("")
//   var vowels = "aeiou";
//   var vowelCount = 0;
//
//   for (var i = 0; i < letters.length; i++) {
//     console.log("OUTER: " + i + ":" + letters[i]);
//     for (var j = 0; j < vowels.length; j++) {
//       console.log("INNER: " + j);
//       // console.log(letters[i]);
//       // console.log(vowels[j]);
//       if (letters[i] == vowels[j]) {
//         var vowel = letters.splice(i, 1); // Mistake: letters[i] instead of just i
//         vowelCount += 1;
//         console.log(vowelCount + ": +" + vowel);
//         // pay attention to methods to be sure they are doing what you think
//         // console.log(letters);
//
//         // return;
//       }
//     }
//
//   }
// }


// v.3 after mentor session

// function countVowels(string) {
//   var letters = string.split('');
//   var vowels = "aeiou";
//   var vowelCount = 0;
//   var j;
//
//     for (var i = 0; i < vowels.length; i++) {
//         console.log("OUTER: " + i + ":" + letters[i]);
//
//         j = 0;
//         while (j < letters.length) {
//         console.log("   INNER: " + i);
//             if (string.includes(vowels[i]) === true) {
//                 var vowelIndex = letters.indexOf(vowels[i]); // Mistake: vowels[i] instead of just i
//                 var vowel = letters.splice(vowelIndex, 1);
//                 // var vowel = letters.splice(indexOf(vowels[i]));
//                 vowelCount++;
//
//                 console.log(vowel);
//                 console.log(vowelCount);
//                 console.log(letters + "\n");
//                 break;  // Stuck here: stop searching for letter?
//             }
//         j++;
//         }
//     }
// }
//
// countVowels("The quick brown fox");


// v4 working version!
function countVowels(string) {
  var letters = string.split('');
  var vowels = "aeiou";
  var vowelIndex;
  var vowel;
  var vowelCount = 0;
  var i = 0;

  while (i < vowels.length) {
    if (letters.includes(vowels[i]) === true) {
      // Is this:
      vowelIndex = letters.indexOf(vowels[i]);
      vowel = letters.splice(vowelIndex, 1);
      // Better than this:
      // vowel = letters.splice(letters.indexOf(vowels[i]), 1);
      vowelCount++;
    }
    else {
      i++;
    }
  }
  console.log(vowelCount);
}
countVowels("The quick brown fox");
