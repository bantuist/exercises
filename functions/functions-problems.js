// 5. 1-2 hours
// Capitalize the first letters
function capitalizeFirstLetter(string) {
	var words = string.split(" "); // We want to find each word & You want to make an array
  var currentWord;
  var bigWords = "";

  for (var i = 0; i < words.length; i++) { // For each word
    currentWord = words[i].split("");
    currentWord.splice(0, 1, currentWord[0].toUpperCase());
    currentWord = currentWord.join("");
    bigWords += currentWord + " ";
  }
  return bigWords;
}
console.log(capitalizeFirstLetter("hi, this is a young kitten"));





/* 6. 20 min
Find the longest word in a string:
      - accepts string
      - split string in separate words
      - loop
      - sets first word as longest word
      - if index in next iteration is longer, sets new longest word
*/

function findLongestWord(string) {
  var words = string.split(" ");
  var longestWord;
  for (var i = 0; i < words.length; i++) {
    if (i == 0) {
      longestWord = words[0];
    }
    else if (words[i].length > longestWord.length) {
      longestWord = words[i];
    }
  }
  return longestWord;
}

console.log(findLongestWord("An elephant never forgets"));





/* 7. 25 min
Count number of vowels in string:
- set up function
- string to array
- loop: for char in string
- if char == vowel
- count++

Solve more efficiently: Switch or nested loop as alternative?
*/
function countVowels(string) {
  var vowelCount = 0;
  var letters = string.toLowerCase().split("");
  for (var i = 0; i < letters.length; i++) {
    if (letters[i] == "a" || letters[i] == "e" || letters[i] == "i" || letters[i] == "o" || letters[i] == "u") {
      vowelCount += 1;
    }
  }
  return vowelCount;
}

console.log(countVowels("The quick brown fox")); // 5





/* 8. 10 min
Check if number is prime:
- 1 2 3 7 13
- loop:
  - divide number by 2 - (n - 1)
  - if result == 0, not prime
  - if result != 0, prime
*/
function checkPrime(num) {
  for (var i = 2; i < num - 1; i++) {
    if (num % i == 0) {
      return num + " is not prime.";
    }
  }
  return num + " is prime."
}
console.log(checkPrime(631)); // prime





// 9. 5 min
// Return argument type

function returnType(arg) {
  var argType = typeof arg;
  console.log(arg + ": " + argType);
}

returnType([1, 2, 3]);  // object
returnType(3);          // number
returnType("hello");    // string
returnType(true);       // boolean
var x;
returnType(x);          // undefined
returnType(returnType); // function





/* 10. 1 hour
Return identity matrix for n x n
(I had to find out what an identity matrix was first)
1 0 0
0 1 0
0 0 1
*/

function idMatrix(num1, num2) {
  var onePlace = 0;
  var one = "1";
  var zero = "0";

  for (var i = 0; i < num1; i++) {
    var row = "";
    for (var j = 0; j < num2; j++) {
      if (j == onePlace) {
        row += one;
      }
      else {
        row += zero;
      }
    }
    onePlace++;
    console.log(row);
  }
}

idMatrix(5, 5);





/* 11. 25 + sleep + 15 min (Easier the next day!)
Find second lowest and second highest numbers in an array

- Helped to find lowest and highest first.
- Sort wasn't working how I expected so I looked it up on w3 and saw that
I needed a compare function to sort numbers. I'm not sure how it works though.
*/

function secLowSecHigh(array) {
  array.sort(function(a, b) {return a-b});
  console.log(array);
  var newArray = [];
  newArray.push(array[1]);
  newArray.push(array[array.length - 2]);
  return newArray;
}

console.log(secLowSecHigh([5, 6, 7, 8, 9, 1, 2, 3, 4, 10, 0]));





/* 12. 10 min
Check if number is perfect
Example : The first perfect number is 6, because 1, 2, and 3 are its proper
positive divisors, and 1 + 2 + 3 = 6. Equivalently, the number 6 is equal to
half the sum of all its positive divisors: ( 1 + 2 + 3 + 6 ) / 2 = 6. The next
perfect number is 28 = 1 + 2 + 4 + 7 + 14. This is followed by the perfect
numbers 496 and 8128.

- find divisiors
- check if sum of divisors = number
- check if number = half sum of divisors

*/

function checkPerfectNumber(num) {
  var sum = 0;
  for (var i = 1; i < num; i++) {
    if (num % i == 0) {
      sum += i;
    }
  }
  if (sum == num) {
    console.log(num + " is perfect.");
  } else {
    console.log(num + " is not perfect.");
  }
}

checkPerfectNumber(4);





// 13. 5 min
// Compute factors of positive integer
function findPositiveFactors(num) {
  var factors = "";
  for (var i = 1; i < num + 1; i++) {
    if (num % i == 0) {
      factors += i + ", ";
    }
  }
  console.log(factors);
}

findPositiveFactors(49);





/* 14. 35 min
Convert amount to coins
- 1 5 10 25
- large to small: if amount / denom
- subtract
*/

function amountToCoins(num) {
  var quarters = 0;
  var dimes = 0;
  var nickels = 0;
  var pennies = 0;

  while (num > 0) {
    if (num >= 25) {
      quarters += 1;
      num -= 25;
    }
    else if (num >= 10) {
      dimes += 1;
      num -= 10;
    }
    else if (num >= 5) {
      nickels += 1;
      num -= 5;
    } else {
      pennies += 1;
      num -= 1;
    }
  }
  console.log("quarters: " + quarters + ", dimes: " + dimes + ", nickels: " + nickels + ", pennies: " + pennies);
}

amountToCoins(223);

// Refactored
function amountToCoins(num) {
  var coins = {quarters: 0, dimes: 0, nickels: 0, pennies: 0};

  if (num > 1) {
    coins.quarters = (Math.floor(num) * 4);
    num = Math.floor(num % Math.floor(num) * 100);
  }
  coins.quarters += Math.floor((num / 100 * 4));
  num = num % 25;
  coins.dimes = Math.floor(num / 10);
  num = num % 10;
  coins.nickels = Math.floor(num / 5);
  num = num % 5;
  coins.pennies = num;
  return coins;
}
console.log(amountToCoins(223.84));





/* 15. 10 min
Compute the value of b^n
*/

function exponentiate(b, n) {
  var product = 1;
  for (var i = 0; i < n; i++) {
    product *= b;
  }
  console.log(product);
}

exponentiate(9, 3);





/* 16. ~ 1 hour
Extract unique characters from a string
Example string : "thequickbrownfoxjumpsoverthelazydog"
Expected Output : "thequickbrownfxjmpsvlazydg"
*/

function extractUniqueChars(string) {
  var letters = string.split("");

  for (var i = 0; i < letters.length; i++) {
    // console.log("outer loop " + i);
    for (var j = i + 1; j < letters.length; j++) {
      // console.log(" inner loop " + j);
      if (letters[i] == letters[j]) {
        // mistake using letters[j] instead of j
        letters.splice(j, 1);
      }
    }
  }
  console.log(letters.join(""));
}

extractUniqueChars("thequickbrownfoxjumpsoverthelazydog"); // thes n





/* 17. 15 min
Get number of letter occurances in a string
- Make more efficient so that it doesn't count the same letter more than once
*/

function counterLetters(string) {
  for (var i = 0; i < string.length; i++) {
    // console.log("outer loop " + i);
    var count = 0;
    for (var j = 0; j < string.length; j++) {
      // console.log(" inner loop " + j);
      if (string[i] == string[j]) {
        count++;
      }
    }
    console.log(string[i].toUpperCase() + "s: " + count);
  }
}

counterLetters("cbcbcac");




/* 18. 1 hour
Search an array with a binary search
- I'm not sure if I did this exactly right, I had to look up what a binary search was.
*/
function numberArray(start, stop) {
  numbers = [];
  for (var i = start; i <= stop; i++) {
    numbers.push(i);
  }
  return numbers;
}

function binarySearch(array, num) {
  array.sort(function(a, b){return a-b});
  // console.log(array);
  var min = 0;
  var max = array.length;

  for (var i = 0; i < array.length; i++) {
    var guess = Math.floor((min + max) / 2);

    console.log("Guess " + i + ": " + array[guess]);
    // console.log("   new min index: " + min);
    // console.log("   new max index: " + max);
    if (array[guess] == num) {
      return array[guess] + " is at index " + guess;
    }
    else if (array[guess] > num) {
      max = guess;
    }
    else {
      min = guess;
    }
  }
}
console.log(binarySearch(numberArray(20, 50), 42));





// 19. 5 min
// Get elements larger than number passed
function getLargerElements(numbers, number) {
  largerNumbers = [];
  for (var i = 0; i < numbers.length; i++) {
    if (numbers[i] > number) {
      largerNumbers.push(numbers[i]);
    }
  }
  console.log(largerNumbers);
}
getLargerElements([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 8);





/* 20. 25 min
Generate a string id (specified length) - what is a string id? - of random characters.

Generate a random string of characters of specified length:
- get a random number in the range of the char list's length
- concat the random number index to random string variable
- repeat specified times

*/
var charList = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

function getRandomString(string, length) {
  var randString = '';
  for (var i = 0; i < length; i++) {
    var randNum = Math.floor(Math.random() * string.length);
    randString += string[randNum];
  }
  console.log(randString);
}

getRandomString(charList, 10);





/* 21. 1-2 hours, no luck
Get all possible subsets with fixed length
e.g. [1, 2, 3] and subset length is 2
returns: [[2, 1], [3, 1], [3, 2], [3, 2, 1]]
*/




/* 22 ~ 5 min
Count number of occurences of letter in string
- loop through string
- if index == letter, count++
-
*/

function countLetters(string, letter) {
  var count = 0;
  for (var i = 0; i < string.length; i++) {
    if (string[i] == letter) {
      count++;
    }
  }
  console.log(count);
}
countLetters("Yosevu Kilonzo", "o");





/* 23. 2 hours, no luck
Find the first non-repeating character in a string.
e.g. 'e' in 'abacddbec'
*/





/* 24. 1 hour, no luck
Apply a bubble sort algorithm
*/





// 25. 5 min
//Return longest country name in array of countries

function longestCountryName(array) {

	for (var i = 0; i < array.length; i++) {
    if (i == 0) {
      var longestName = array[0];
    } else if (array[i].length > longestName.length){
      longestName = array[i];
    }
  }
  console.log(longestName);
}
longestCountryName(["Australia", "Germany", "United States of America"]);





/* 26. 1-2 hours
Find the longest substring without repeating characters
- Sorry this is especially ugly!
*/
function longestSubstring(string) {
  var substring = "";
  var longest = "";
  for (var i = 0; i < string.length; i++) {
    // console.log("LOOP:" + i);
    if (string[i] != string[i + 1]) {
      if (i == 0) {
        substring += string[i];
      }
      else if (i == string.length - 1) { // -1?
        break;
      }
      substring += string[i + 1];
      if (substring.length > longest.length) {
        longest = substring;
      }

    } else {
        substring = string[i + 1];
    }
  }
  console.log("longest:: " + longest);
}

longestSubstring("Food and beer in the outdoor swimming pool"); // "er in the out do"





// 27. Still unattempted
// Return longest palindrome in a string




// 28. 5 min
// Pass a function as a parameter

function squareNumber(num) {
  console.log(num * num);
}

function passFunction(functionPassed) {
  functionPassed;
}

passFunction(squareNumber(8));

Pass a function as a parameter

// Refactor






/* 29. 25 min
Get a functions name

- Am I supposed to do something like this? I don't really understand this one.
*/

// var numberArray = function(start, stop) {
//   numbers = [];
//   for (var i = start; i <= stop; i++) {
//     numbers.push(i);
//   }
// }

function numberArray(start, stop) {
  numbers = [];
  for (var i = start; i <= stop; i++) {
    numbers.push(i);
  }
}

function getFunctionName(passedFunction) {
  var name = "";
  var functionString = passedFunction.toString();
  for (var i = 0; i < functionString.length; i++) {
    if (i > 8) {
      if (functionString[i] == "(") {
        break;
      } else {
        name += functionString[i];
      }
    }
  }
  console.log(name);
}

getFunctionName(numberArray);
