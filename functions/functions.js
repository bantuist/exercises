// http://www.w3resource.com/javascript-exercises/javascript-functions-exercises.php

// 1 ~ 10 min
function reverseNumber(num) {
  return num.toString().split("").reverse().join(""); // Number()
}

console.log(reverseNumber(12345));

// 2 ~ 15 min

function checkPalindrome(word) {
  word = word.replace(" ", "");
  var wordReversed = word.replace(" ", "").split("").reverse().join("");
  if (word == wordReversed) {
    console.log("Palindrome: \"" + word + "\" is \"" + wordReversed + "\" backwards.")
  }
  else {
    console.log("Not a palindrome: \"" + word + "\" is \"" + wordReversed + "\" backwards.")
  }
}

checkPalindrome("nurses run");

/* Caila's code
word = word.replace(" ", "");
j = word.length - 1
i = 0
while i != j {
	if word.charAt(i) != word.charAt(j)
  	return "not a palindrome";
}
return "is a palindrome";

*/

// 3 ~ 25+ min // I knew I needed nested loops, I just couldn't figure out how to set them up.
// Typin this out helped: /
// outer loop:  d
//  inner loop: d+o d+o+g
// outer loop:  o
//  inner loop: o+g
// outer loop: g
function generateStrings(string) {
  for (var i = 0; i < string.length; i++) {
    var currentString = string[i];
    console.log(currentString);
    for (var j = i + 1; j < string.length; j++) {
      currentString += string[j];
      console.log(currentString);
    }
  }

}

console.log(generateStrings("dog"));

// 4 ~ 5 min
function orderLetters(string) {
  return string.split("").sort().join(""); // do without sort()?
}
console.log(orderLetters("dcba"));

// 5 ~ in progress
/*function capitalize(string) {
  for (var i = 0; i < string.length; i++) {
    if (string[i] == 0 || string[i] == " ") {
      string = string.replace(string[i + 1], string[i + 1].toUpperCase());
    }
  }
  return string;
}
console.log(capitalize("hi, this is a young kitten"));
*/
function capitalize(string) {
	var words = string.split(" "); //We want to find each word & You want to make an array
  for (var i = 0; i < words.length; i++) { //For each word
  	string.replace(string.charAt(0), string.charAt(0).toUpperCase()); //You want the first letter charAt(0)

    capitalizeFirstLetter(words[i]); //You want to capitalize this letter
  }
  //You want to make a sentence
  return string;
}
function capitalizeFirstLetter(word){
  Word.charArt(0).toUpperCase
}

// New

// 5 ~ in progress 25 + 25 + 25 + 15
/*function capitalize(string) {
  for (var i = 0; i < string.length; i++) {
    if (string[i] == 0 || string[i] == " ") {
      string = string.replace(string[i + 1], string[i + 1].toUpperCase());
    }
  }
  return string;
}
console.log(capitalize("hi, this is a young kitten"));
*/

function capitalizeFirstLetter(string) {
	var words = string.split(" "); // We want to find each word & You want to make an array
  var currentWord;
  var bigWords = "";

  for (var i = 0; i < words.length; i++) { // For each word
    currentWord = words[i].split("");
    currentWord.splice(0, 1, currentWord[0].toUpperCase());
    currentWord = currentWord.join("");
    bigWords += currentWord + " ";
  }
  return bigWords;
}
console.log(capitalizeFirstLetter("hi, this is a young kitten"));


// var firstLetter = words[i].charAt(0);
// words[i].replace(RegExp(firstLetter, "g"), capitalize(words[i].charAt(0)));
// console.log(words[i]);
// bigFirstLetter = words[i].charAt(0).toUpperCase();
// console.log(smallFirstLetter + " " + bigFirstLetter);
// words[i].replace(smallFirstLetter, bigFirstLetter); // Why can't I pass a variable to replace? // Regex
// string = string.replace(words[i].charAt(0), words[i].charAt(0).toUpperCase()); //You want the first letter charAt(0)
// console.log(words[i]);

/*
// capitalizeFirstLetter(words[i]); //You want to capitalize this letter
// function capitalizeFirstLetter(word){
//   Word.charArt(0).toUpperCase


*/


/*  6 ~ 20 min
      Write a JavaScript function that accepts a string as a parameter and find the longest word within the string.
      Example string : 'Web Development Tutorial'
      Expected Output : 'Development'

      - accepts string
      - split string in separate words
      - loop
      - sets first word as longest word
      - if index in next iteration is longer, sets new longest word
*/

function findLongestWord(string) {
  var words = string.split(" ");
  var longestWord;
  for (var i = 0; i < words.length; i++) {
    if (i == 0) {
      longestWord = words[0];
    }
    else if (words[i].length > longestWord.length) {
      longestWord = words[i];
    }
  }
  return longestWord;
}

console.log(findLongestWord("An elephant never forgets"));

/*

7. ~ 25+ min Write a JavaScript function that accepts a string as a parameter and counts the number of vowels within the string.
Note : As the letter 'y' can be regarded as both a vowel and a consonant, we do not count 'y' as vowel here.
Example string : 'The quick brown fox'
Expected Output : 5

- set up function
- string to array
- loop: for char in string
- if char == vowel
- count++
*/

// First attempt

// function countVowels(string) {
//   var vowelCount = 0;
//   var vowels = ["a", "e", "i", "o", "u"];
//   letters = string.toLowerCase().split("");
//   // console.log(letters);
//   for (var i = 0; i < letters.length; i++) {
//     for (var j = 0; j < vowels.length; i++) {
//       if (letters[i] == vowels[j]) {
//         vowelCount += 1;
//         console.log(vowelCount);
//       }
//     }
//   }
//   return vowelCount;
//
// }

// Second attempt
// Solve more efficiently: Switch or nested loop as alternative?

function countVowels(string) {
  var vowelCount = 0;
  var letters = string.toLowerCase().split("");
  for (var i = 0; i < letters.length; i++) {
    if (letters[i] == "a" || letters[i] == "e" || letters[i] == "i" || letters[i] == "o" || letters[i] == "u") {
      vowelCount += 1;
    }
  }
  return vowelCount;
}

console.log(countVowels("An elephant never forgets"));


/* 8. ~ 10 min
Write a JavaScript function that accepts a number as a parameter and check the number is prime or not.
Note : A prime number (or a prime) is a natural number greater than 1 that has no positive divisors other than 1 and itself.

- 1 2 3 7 13
- loop:
  - divide number by 2 - (n - 1)
  - if result == 0, not prime
  - if result != 0, prime
*/

function checkPrime(num) {
  for (var i = 2; i < num - 1; i++) {
    if (num % i == 0) {
      return num + " is not prime.";
    }
  }
  return num + " is prime."
}
console.log(checkPrime(631));

/*
9. ~ 5 min
Write a JavaScript function which accepts an argument and returns the type.
Note : There are six possible values that typeof returns: object, boolean, function, number, string, and undefined.

*/

function returnType(arg) {
  var argType = typeof arg;
  console.log(arg + " is " + argType);
}

returnType([1, 2, 3]);
returnType(3);
returnType("hello");
returnType(true);
var x;
returnType(x);
returnType(returnType);


/* 25 min in progress
10. Write a JavaScript function which returns the n rows by n columns identity matrix.
- 3 x 3
1 0 0
0 1 0
0 0 1
- if [0][0], [1][1], [2][2]
- for column
*/
// Snapshot 1
// function identidyMatrix(num1, num2) {
//   for (var i = 0; i < num1; i++) {
//     for (var j = 0; j < num2; j++) {
//       if (i == j) {
//         console.log(1);
//       } else {
//         console.log(0);
//       }
//     }
//   }
// }
//
// console.log(identidyMatrix(3, 3));

// Snapshot 2

function identidyMatrix(num1, num2) {

  for (var i = 0; i < num1; i++) {
    for (var j = 0; j < num2; j++) {

      if (i == j) {
        console.log(1);
      } else {
        console.log(0);
      }
    }
  }
}

console.log(identidyMatrix(3, 3));
