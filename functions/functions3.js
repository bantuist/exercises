/* 22 ~ 5 min
Write a JavaScript function that accepts two arguments, a string and a letter
and the function will count the number of occurrences of the specified letter within the string.
Sample arguments : 'w3resource.com', 'o'
Expected output : 2
- loop through string
- if index == letter, count++
*/

function countLetters(string, letter) {
  var count = 0;
  for (var i = 0; i < string.length; i++) {
    if (string[i] == letter) {
      count++;
    }
  }
  console.log(count);
}

countLetters("Yosevu Kilonzo", "o");

/* 23. ~ hard
Write a JavaScript function to find the first not repeated character.
Sample arguments : 'abacddbec'
Expected output : 'e'


*/


/* 24. ~ hard
Write a JavaScript function to apply Bubble Sort algorithm.
Note : According to wikipedia "Bubble sort, sometimes referred to as sinking sort,
is a simple sorting algorithm that works by repeatedly stepping through the list to be sorted,
comparing each pair of adjacent items and swapping them if they are in the wrong order".
Sample array : [12, 345, 4, 546, 122, 84, 98, 64, 9, 1, 3223, 455, 23, 234, 213]
Expected output : [3223, 546, 455, 345, 234, 213, 122, 98, 84, 64, 23, 12, 9, 4, 1]

*/

/* 25. ~ 5 min
Write a JavaScript function that accepts a list of country names as input and
returns the longest country name as output.
Sample function : Longest_Country_Name(["Australia", "Germany", "United States of America"])
Expected output : "United States of America"
*/

function longestCountryName(array) {
  for (var i = 0; i < array.length; i++) {
    if (i == 0) {
      var longestName = array[0];
    } else if (array[i].length > longestName.length){
      longestName = array[i];
    }
  }
  console.log(longestName);
}
longestCountryName(["Australia", "Germany", "United States of America"]);

/* 26. ~ hard
Write a JavaScript function to find longest substring in a given a string without repeating characters

*/

/* 27. ~ medium
Write a JavaScript function that returns the longest palindrome in a given string.
- See note

*/




/* ~ 5
28. Write a JavaScript program to pass a 'JavaScript function' as parameter.

*/
function squareNumber(num) {
  console.log(num * num);
}

function passFunction(functionPassed) {
  functionPassed;
}

passFunction(squareNumber(8));


/* ~

29. Write a JavaScript function to get the function name.

*/
