| Checkpoint | Kata                                                                                                                                                                                                                      |
|:-----------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [One]()    | - [Jenny’s Secret Message](#jennys-secret-message)<br>- [Count by X](#count-by-x)<br>- [Stringy Strings](#stringy-strings)<br>- [Bonus Challenge - Find the Sum](#find-the-sum)                                           |
| [Two]()    | - [Tube Strike Options](#tube-strike-options)<br>- [Character Counter](#character-counter)<br>- [Most Frequent Item Count](#most-frequent-item-count)<br>- [Bonus Challenge - Word A10n Abbreviation](#word-abbreviation) |
| [Three]()  | - [Longest Palindrome](#longest-palindrome)<br>- [Sequence Generator](#sequence-generator)<br>- [Checkered Board](#checkered-board)<br>- [Bonus Challenge - Sudoku Solution Validator](#sudoku-solution-validator)                                                         |

---

<a name="jennys-secret-message"></a>
### [Jenny’s Secret Message](http://www.codewars.com/kata/jennys-secret-message/solutions/ruby)

```ruby
def greet(name)
return "Hello, my love!" if name == "Johnny"
  return "Hello, #{name}!"
end
```

---

<a name="count-by-x"></a>
### [Count by X](http://www.codewars.com/kata/count-by-x/solutions/ruby)

```ruby
def count_by(x, n)
  Array.new(n) { |i| x * (i + 1) }
end
```

---

<a name="stringy-strings"></a>
### [Stringy Strings](http://www.codewars.com/kata/stringy-strings/train/ruby)

```ruby
def stringy(size)
  out = ''
  size.times { |i| out << (i % 2 == 0 ? '1' : '0') }
  out
end
```

---

<a name="tube-strike-options"></a>
### [Tube Strike Options](http://www.codewars.com/kata/tube-strike-options-calculator/solutions/ruby)

```ruby
def calculator(distance, bus_drive, bus_walk)
  #your code here, use the preset $walk and $bus values
  walk_time = distance / $walk
  bus_time = (bus_drive / $bus) + (bus_walk / $walk)
  return "Bus" if walk_time > 2
  return "Walk" if walk_time <= 1.0 / 6.0
  return [walk_time, bus_time].min == walk_time ? "Walk" : "Bus"
end
```

---

<a name="character-counter"></a>
### [Character Counter](http://www.codewars.com/kata/character-counter/solutions/ruby)

```ruby
def validate_word(word)
  count_hash = {}
  value = 0
  word.downcase.each_char { |char|
    value = (count_hash[char] || 0) + 1
    count_hash[char] = value
  }
  return (count_hash.select { |k, v| v != value }).size == 0
end
```

---

<a name="most-frequent-item-count"></a>
### [Most Frequent Item Count](http://www.codewars.com/kata/find-count-of-most-frequent-item-in-an-array/solutions/ruby)

```ruby
def most_frequent_item_count(collection)
  longest_run = 0
  current_run = 0
  collection.sort!.each_index { |index|
    current_run += 1
    current_run = 1 if index == 0 || collection[index] != collection[index - 1]
    longest_run = [longest_run, current_run].max
  }
  longest_run
end
```

---

<a name="longest-palindrome"></a>
### [Longest Palindrome](http://www.codewars.com/kata/longest-palindrome/solutions/ruby)

```ruby
def longest_palindrome s
  longest = 0
  current = 0
  stack = []
  s.each_char { |char|
    if char == stack.last
      stack.pop
      current = current > 1 ? current + 2 : 2
    elsif char == stack[-2]
      stack.pop(2)
      current = 3
    else
      stack.push(char)
      current = 1
    end
    longest = [longest, current].max
  }
  longest
end
```

---

<a name="sequence-generator"></a>
### [Sequence Generator](http://www.codewars.com/kata/sequence-generator/solutions/ruby)

```ruby
def sequence_gen(*args)
  Enumerator.new do |yielder|
    loop do
      args.push(args.inject(:+))
      yielder << args.shift
    end
  end
end
```

---

<a name="checkered-board"></a>
### [Checkered Board](http://www.codewars.com/kata/checkered-board/solutions/ruby)

```ruby
def checkered_board(dimension)
  return false if !dimension.is_a?(Integer) || dimension < 2
  out = ""
  dark_square = "\u25A1"
  lite_square = "\u25A0"
  square_one = dimension % 2 == 0 ? dark_square : lite_square
  square_two = square_one == dark_square ? lite_square : dark_square
  dimension.times { |i|
    dimension.times { |j|
      out += j % 2 == 0 ? square_one : square_two
      out += " " if j < (dimension - 1)
    }
    out += "\n" if i < (dimension - 1)
    temp = square_one
    square_one = square_two
    square_two = temp
  }
  out.encode("UTF-8")
end
```

---

<a name="multiply"></a>
### [Multiply](http://www.codewars.com/kata/multiply/train/ruby)

```ruby
def multiply(a, b)
  a * b
end
```

The only change required is the `,` that separates the parameter variables.

---

<a name="broken-greetings"></a>
### [Broken Greetings](http://www.codewars.com/kata/multiply/train/ruby)

```ruby
class Person
  def initialize(name)
    @name = name
  end

  def greet(other_name)
    return "Hi #{other_name}, my name is #{@name}"
  end
end
```

The key to solving Broken Greetings is recognizing that the `name` attribute is not properly accessed within `greet`. Adding an `@` lets Ruby access the instance variable.

---

<a name="fizzbuzz"></a>
### [FizzBuzz](http://www.codewars.com/kata/fizz-buzz/train/ruby)

The modulus operator is key to unlocking the FizzBuzz solution. The remaining pain points involve creating the array from `1` to `n`, students may use a more traditional `for` loop and shovel operator instead.

```ruby
# return an array
def fizzbuzz(n)
  (1..n).to_a.map do |value|
    mod3 = value % 3 == 0
    mod5 = value % 5 == 0
    if mod3 && mod5
      'FizzBuzz'
    elsif mod3
      'Fizz'
    elsif mod5
      'Buzz'
    else
      value
    end
  end
end
```

---

<a name="musical-pitch-classes"></a>
### [Musical Pitch Classes](http://www.codewars.com/kata/musical-pitch-classes/train/ruby)

In Musical Pitch Classes, the key is to convert the note to a numeric value. We do this by creating two arrays and using the index of one to discover a value in the other. The possible pain points here involve pulling the first character from the `note` parameter. This is done by indexing the string like an array, `note[0]` for example.

```ruby
def pitch_class(note)
  note_values = [0, 2, 4, 5, 7, 9, 11]
  note_indices = %w(C D E F G A B)
  if note == nil || note.length > 2 || note_indices.index(note[0]) == nil
    return nil
  end
  if (note[1] != nil && (note[1] != '#' && note[1] != 'b'))
    return nil
  end
  note_index = note_indices.index(note[0])
  note_value = note_values[note_index]
  note_value += 1 unless note[1] != '#'
  note_value -= 1 unless note[1] != 'b'
  note_value %= 12
end
```

---

<a name="find-the-sum"></a>
### [Find the Sum](http://www.codewars.com/kata/find-sum-of-top-left-to-bottom-right-diagonals/train/ruby)

The trick is for students to identify the pattern and recognize that they must iterate across a collection of arrays. A more traditional `for` loop may be employed here with two-dimensional indexing.

```ruby
def diagonal_sum(matrix)
  sum = 0
  matrix.each_with_index do |array, index|
    sum += array[index]
  end
  sum
end
```

---

<a name="vasya-and-stairs"></a>
### [Vasya and Stairs](http://www.codewars.com/kata/vasya-and-stairs/train/ruby)

This is a tricky problem to understand, but not too difficult to solve once you understand the constraints. This example begins with the maximum number of two-steps and minimum number of one-steps. It loops, exchanging two-steps for two one-steps until it finds a combination that is evenly divisible by `m`.

```ruby
def number_of_steps(steps, m)
  two_steps = (steps / 2).round(0)
  one_steps = steps % 2
  while ((two_steps + one_steps) % m != 0 && two_steps > -1) do
    two_steps -= 1
    one_steps += 2
  end
  return two_steps + one_steps unless two_steps < 0
  -1
end
```

---

<a name="enigma-machine-part-1"></a>
### [The Enigma Machine - Part 1: The Plugboard](http://www.codewars.com/kata/the-enigma-machine-part-1-the-plugboard/train/ruby)

The key challenge in this Kata is to detect the failure states and raise the appropriate errors. The code may raise any kind of error, as long as it does so based on the circumstances. The tests will not pass until failure cases detect errors.

Our solution uses a scan to divide `wires` into character pairs, but students may just as well use a traditional loop structure.

```ruby
class Plugboard
  attr_accessor :wire_hash

  def initialize(wires = nil)
    if wires == nil
      return
    end
  	raise(ArgumentError, "Invalid length") unless (wires.length % 2) == 0
    @wire_hash = {}
    pairs = wires.scan(/..?/)
    raise(ArgumentError, "Too many pairs") unless pairs.length < 11
    pairs.each do |pair|
      char_one = pair[0] unless @wire_hash.keys.include?(pair[0]) || pair[0] < 'A' || pair[0] > 'Z'
      char_two = pair[1] unless @wire_hash.keys.include?(pair[1]) || pair[1] < 'A' || pair[1] > 'Z'
      raise(ArgumentError, "Invalid pairs") unless char_one != nil && char_two != nil
      @wire_hash[char_one] = char_two
      @wire_hash[char_two] = char_one
    end
  end

  def process(wire)
    if @wire_hash.keys.include?(wire)
      return @wire_hash[wire]
    end
    wire
  end
end
```

---

<a name="format-a-list-of-names"></a>
### [Format a List of Names](http://www.codewars.com/kata/format-a-string-of-names-like-bart-lisa-and-maggie/train/ruby)

This exercise challenges the student's string manipulation skills. This particular solution is straight forward Ruby.

```ruby
def list_names(names)
  if names.nil || names.empty?
    return ''
  elsif names.length == 1
    return names[0][:name]
  end
  last_name = names.pop
  second_to_last = names.pop
  out_string = ''
  names.each do |value|
    out_string += value[:name] + ', '
  end
  return out_string + second_to_last[:name] + ' & ' + last_name[:name]
end
```

---

<a name="word-abbreviation"></a>
### [Word Abbreviation (A10n)](http://www.codewars.com/kata/word-a10n-abbreviation/train/ruby)

This is one of the most challenging Kata the student will face. Our solution involved dividing the original string into words using the `\w+{4,}` expression, then replacing each word in the original string. Students may regenerate the string instead using nested loops instead. Other solutions exist as well.

```ruby
class Abbreviator
  def self.abbreviate(string)
    string.scan(/\w{4,}/).each do |word|
      abbreviation = word[0]
      abbreviation << (word.length - 2).to_s
      abbreviation << word[-1]
      string.sub!(word, abbreviation)
    end
    string
  end
end
```

---

<a name="sudoku-solution-validator"></a>
### [Sudoku Solution Validator](http://www.codewars.com/kata/sudoku-solution-validator/train/ruby)

For our Sudoku validator, we chose to divide the work into three functions. Each function handles the validation of its respective type (row, column, or square).

```ruby
def valid_solution(board)
  for i in 0..8
    return false unless valid_column?(board, i)
    return false unless valid_row?(board, i)
    return false unless valid_square?(board, i)
  end
  true
end

def valid_column?(board, column)
  numbers = (1..9).to_a
  for i in 0..8
    numbers.delete(board[i][column])
  end
  numbers.count == 0
end

def valid_row?(board, row)
  numbers = (1..9).to_a
  for i in 0..8
    numbers.delete(board[row][i])
  end
  numbers.count == 0
end

def valid_square?(board, square)
  numbers = (1..9).to_a
  starting_rows = [0,0,0,3,3,3,6,6,6]
  starting_cols = [0,3,6,0,3,6,0,3,6]
  row = starting_rows[square]
  col = starting_cols[square]
  for i in 0..2
    for j in 0..2
      numbers.delete(board[row + j][col + i])
    end
  end
  numbers.count == 0
end
```

---

<a name="validate-sudoku-with-size"></a>
### [Validate Sudoku with Size ‘N×N’](http://www.codewars.com/kata/validate-sudoku-with-size-nxn/train/ruby)

This solution adapts our previous solution to work on any square-sized Sudoku board. It first verifies accurate dimensions (`N×N`), and the remaining methods have remained unchanged except for `valid_square?`. This method requires performing a square root to determine the size of the quadrant to validate.

```ruby
class Sudoku
  attr_accessor :board
  attr_accessor :valid_dims
  attr_accessor :dimension

  def initialize(board = [])
    @board = board
    @dimension = board.count
    @valid_dims = board.reduce(0) { |sum, row| sum + row.count } == (@dimension * @dimension)
  end

  def is_valid
    return false unless @valid_dims
    for i in 0...@dimension
      return false unless valid_column?(i)
      return false unless valid_row?(i)
      return false unless valid_square?(i)
    end
    true
  end

  def valid_column?(column)
    numbers = (1..@dimension).to_a
    for i in 0...@dimension
      numbers.delete(@board[i][column])
    end
    numbers.count == 0
  end

  def valid_row?(row)
    numbers = (1..@dimension).to_a
    for i in 0...@dimension
      numbers.delete(@board[row][i])
    end
    numbers.count == 0
  end

  def valid_square?(square)
    numbers = (1..@dimension).to_a
    root_dimension = Math::sqrt(@dimension)
    row = (square / root_dimension).floor * root_dimension
    col = (square % root_dimension) * root_dimension
    for i in 0...root_dimension
      for j in 0...root_dimension
        numbers.delete(@board[row + j][col + i])
      end
    end
    numbers.count == 0
  end
end
```

---

<a name="text-align-justify"></a>
### [Text Align Justify](http://www.codewars.com/kata/text-align-justify/train/ruby)

This Kata is exceptionally challenging as it provides a vast number of constraints, most of which require nested conditional verification. Here's our solution:

```ruby
def justify(text, width)
  # Split by white space
  words = text.split(/[ ]/)
  all_lines = []
  while words.count > 0
    line_array = []
    char_count = 0
    space_count = 0
    while words.count > 0 && (char_count + space_count + words.first.length) <= width
      char_count += words.first.length
      line_array.push(words.shift)
      space_count += 1
    end

    # Return if we're out of words
    return (all_lines << line_array.join(" ")).join("\n") if words.count == 0

    # Justify the line

    # One-word only
    all_lines << line_array.shift if line_array.count == 1
    next if line_array.count == 0

    # Multiple words
    white_space_count = width - char_count
    gap_count = line_array.count - 1
    largest_gap = (white_space_count / gap_count).to_i + 1
    large_gaps = gap_count
    gap_array = Array.new(gap_count) {|i| largest_gap}
    until gap_array.inject(:+) == white_space_count
      large_gaps -= 1
      if large_gaps < 0
        largest_gap -= 1
        large_gaps = gap_count
      end
      gap_array = Array.new(gap_count) {|i| i < large_gaps ? largest_gap : largest_gap - 1}
    end
    line_array.map! { |word|
      gap_array.empty? ? word : word.ljust(word.length + gap_array.shift)
    }
    all_lines << line_array.join
  end  
end
```

This solution first divides the text by white space and places it into an array of words (contiguous characters). Then it builds the result (`final_string`) one line at a time by removing words from the array until the maximum width is reached.

After discovering the number of words that fit in the current line, this method determines the number of spaces required between each of these words. It does so by assuming the widest gap first (width - number of characters in the line), then whittling down until a combination of large and small gaps fill the width of the line.

Once deduced, it appends each word to `final_string`, separated by the appropriate amount of spaces. Lastly, it caps each line off with a `"\n"`.

---

<a name="find-the-most-frequent-words"></a>
### [Find the Most Frequent Words](http://www.codewars.com/kata/most-frequently-used-words-in-a-text/train/ruby)

Our solution iterates over each unique word, and adds each occurrence of it to a hash. Then we look for words that had the largest repeated value, ties broken arbitrarily. This solution avoids creating an array and avoids sorting the entire collection of words.

```ruby
def top_3_words(text)
  trimmed_text = text.downcase
  most_appearances = 0
  all_words = {}
  trimmed_text.scan(/\w+'?\w?/) { |match|
    appearances = all_words[match] || 0
    all_words[match] = appearances + 1
		most_appearances = [most_appearances, appearances + 1].max
  }
  top_words = []
  while top_words.count < 3 && !all_words.empty? do
    word = all_words.key(most_appearances)
    while word != nil && top_words.count < 3 do
      top_words << word
      all_words.delete(word)
      word = all_words.key(most_appearances)
    end
    most_appearances -= 1
  end
  top_words
end
```

The trick here is to spot that `most_appearances` is not deprecated until we exit the inner `while` loop. That loop assumes that there may be more than one key whose value is of `most_appearances`, thus breaking ties arbitrarily.

---

<a name="discover-a-secret-string"></a>
### [Discover a Secret String](http://www.codewars.com/kata/recover-a-secret-string-from-random-triplets/train/ruby)

This is the most difficult Kata presented to students during their Rails foundation. I personally slaved away at this problem for three days before coming to the conclusion you see here. If your student solves this in any reasonable amount of time, we will award them a medal.

The basis of this solution is to rearrange the characters in the final string (`secret_string`) until _all_ of the conditions demanded by the triplets are satisfied. It does this by removing the first triplet, then iterating through the remainder in search of a triplet that repeats two of the three characters found in the first triplet. If none match, it assumes the second triplet and the loop repeats.

Once it finds a match, the original triplet grows in length by calculated insertion. After insertion, it compares the order within `secret_string` to the order provided by the current triplet. If they do not match, one of two alterations occur: the middle moves to the front or the back moves to the middle. This pattern repeats until it satisfies every triplet.

```ruby
def recover_secret triplets
  secret_string = triplets.shift
  loops = triplets.length
  number_satisfied = 0
  while (number_satisfied < triplets.length) do
    loops -= 1
    triplet = triplets.shift
    triplets.push(triplet)

    first_index = secret_string.index(triplet[0])
    second_index = secret_string.index(triplet[1])
    third_index = secret_string.index(triplet[2])

    new_chars = 0
    new_chars += 1 if first_index == nil
    new_chars += 1 if second_index == nil
    new_chars += 1 if third_index == nil

    if new_chars == 1
      if first_index == nil
        secret_string.insert(second_index, triplet[0])
      elsif second_index == nil
        secret_string.insert(third_index, triplet[1])
      elsif third_index == nil
        secret_string.insert(second_index + 1, triplet[2])
      end
      number_satisfied += 1
    elsif new_chars > 1 && loops <= 0 && secret_string.length == 3
      # Nothing matches with the first triplet
			triplets.push(secret_string)
      secret_string = triplets.shift
      loops = triplets.length
      number_satisfied = 0
    end

    # Move on if we inserted a new character or had more than one unknown
    next if new_chars > 0

    first_index = secret_string.index(triplet[0])
    second_index = secret_string.index(triplet[1])
    third_index = secret_string.index(triplet[2])
    # Check for accurate order
    indices = [first_index, second_index, third_index]
    sorted_indices = indices.sort
    if indices == sorted_indices
      number_satisfied += 1
      next
    end
    # Re-order
    index_to_move = indices[0] == sorted_indices[0] ? 1 : 0
    char_to_move = secret_string.delete_at(indices[index_to_move])
    secret_string.insert(indices[index_to_move + 1], char_to_move)
		number_satisfied = 0
  end
  secret_string.join
end
```
