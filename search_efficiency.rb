# binary search
def binary_search(array, number)
  start_time = Time.now
  first = 0
  last = array.length - 1

  while (first <= last)
    middle = first + ((last - first) / 2)

    if array[middle] == number
      end_time = Time.now
      puts "binary: #{(end_time.to_f - start_time.to_f).round(5) * 1000}ms"
      return middle
    elsif array[middle] < number
      first = middle + 1
    else
      last = middle - 1
    end
  end
  nil
end


# iterative search
def iterative_search(array, number)
  start_time = Time.now
  array.each do |i|
    if array[i] == number
      end_time = Time.now
      puts "iterative: #{(end_time.to_f - start_time.to_f).round(5) * 1000}ms"
      return i
    end
  end
  nil
end

a = 1..1000000
a = a.to_a
puts "key index: #{binary_search(a, 1000000)}"
puts "key index: #{iterative_search(a, 1000000)}"

