function findBall(scales) {
  // call scales.getWeight() max 4 times
  // return indexOfHeavyBall;
  var ballArray = [];
  var leftPan = [];
  var rightPan = [];
  var w;

  for (var i = 0; i < 8; i++) {
    ballArray.push(i);
  }
  while (ballArray.length > 1) {
    // Mistake: leftPan = ballArray.splice(0, 8 / 2);
    leftPan = ballArray.splice(0, ballArray.length / 2);
    rightPan = ballArray;
    w = scales.getWeight(leftPan, rightPan);

    if (w === -1) { // left pan is heavier
      ballArray = leftPan;
    }

    if (w === 1) { // right pan is heavier
      ballArray = rightPan;
    }
  }
  return ballArray[0];
}

/* heavy 1
ballArray is [0, 1, 2, 3, 4, 5, 6, 7]
1. ballArray is [0, 1, 2, 3] vs [4, 5, 6, 7]
2. ballArray is [0, 1] vs [2, 3]
3. ballArray is [0] vs [1]
*/
